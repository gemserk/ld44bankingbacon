﻿using Gemserk.LD44.Game.UI;
using UnityEngine;

public class GameOutcomeSceneController : MonoBehaviour
{
    [SerializeField]
    private GameOutcomeScreen _gameOutcome;

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            _gameOutcome.OnGameCompleted(new GameOutcome
            {
                playersData = new []
                {
                    new GameOutcomeData
                    {
                        player = 0,
                        win = false
                    }, 
                    new GameOutcomeData
                    {
                        player = 1,
                        win = true
                    }
                }
            }, delegate
            {
                Debug.Log("On Restart!");
            });
        }
    }
}
