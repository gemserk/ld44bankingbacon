﻿using UnityEngine;

public class TestModelController : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private Camera _camera;
    
    // Update is called once per frame
    private void Update()
    {
        var running = Input.GetKey(KeyCode.Space);
        _animator.SetBool("Running", running);

       // var position = _camera.ScreenToWorldPoint(Input.mousePosition);
        var ray = _camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var r))
        {
            _animator.transform.LookAt(r.point);
        }
    }
}
