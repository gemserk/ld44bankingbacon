﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraLookAtFixed : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;

    [SerializeField]
    private Transform _transform;

    [SerializeField]
    private Vector3 _up;

    // Update is called once per frame
    private void Update()
    {
        if (_camera == null || _transform == null)
            return;
        
        _camera.transform.LookAt(_transform, _up);
    }
}
