using UnityEngine;

namespace Gemserk.LD44.Helpers.Actions
{
    public class SpawnParticleAction : MonoBehaviour
    {
        public GameObject go;
        public bool forceUp;
        
        public void Execute(Transform where)
        {
            GameObject.Instantiate(go, where.position, forceUp ? Quaternion.identity : where.rotation);
        }
    }
}