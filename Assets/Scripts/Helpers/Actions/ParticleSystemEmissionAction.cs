using UnityEngine;

namespace Gemserk.LD44.Helpers.Actions
{
    public class ParticleSystemEmissionAction : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem _particleSystem;

        public bool withChildren;
        
        public void Execute(bool enabled)
        {
            if (enabled)
            {
                _particleSystem.Play(withChildren);
            }
            else
            {
                _particleSystem.Stop(withChildren, ParticleSystemStopBehavior.StopEmitting);
            }
        }
    }
}