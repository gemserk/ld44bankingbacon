using UnityEngine;

namespace Gemserk.LD44.Helpers.Actions
{
    public class DestroyGOAction : MonoBehaviour
    {
        public void Execute(GameObject go)
        {
            GameObject.Destroy(go);
        }
    }
}