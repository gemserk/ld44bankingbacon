using UnityEngine;

namespace Gemserk.LD44.Helpers.Actions
{
    public class LogAction : MonoBehaviour
    {
        public void Execute(string message)
        {
            Debug.Log(message);
        }
    }
}