using System;
using UnityEngine;
using UnityEngine.Events;

namespace UnityTemplateProjects.Helpers
{
    [Serializable]
    public class ColliderEvent : UnityEvent<Collider>{}
    
    
    public class CollisionHandler : MonoBehaviour
    {
        public ColliderEvent onTriggerEnterHandler;
        
        private void OnTriggerEnter(Collider other)
        {
            onTriggerEnterHandler.Invoke(other);
        }
    }
}