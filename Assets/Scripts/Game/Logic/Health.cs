﻿

using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    public class Health 
    {
        private float _current;
        private float _max;

        public void SetMax(float max)
        {
            _max = max;
            _current = max;
        }

        public float Current
        {
            get => _current;
            set => _current = value;
        }

        public void Add(float delta)
        {
            _current += delta;
            _current = Mathf.Min(_max, Mathf.Max(0, _current));
        }
    }
}
