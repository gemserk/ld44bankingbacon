using UnityEditor;
using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    [CreateAssetMenu(menuName = "Gemserk/Controller Input")]
    public class UnitControllerInputAsset : ScriptableObject
    {
        public string moveHorizontalAxis;
        public string moveVerticalAxis;
    
        // public string fireHorizontalAxis;
        // public string fireVerticalAxis;
    
        public string fire1Button;
        public string fire2Button;

        [ContextMenu("Preconfigure")]
        public void Preconfigure()
        {
            moveHorizontalAxis = $"{name}_MoveHorizontal";
            moveVerticalAxis = $"{name}_MoveVertical";
           // fireHorizontalAxis = $"{name}_FireHorizontal";
           // fireVerticalAxis = $"{name}_FireVertical";
            fire1Button = $"{name}_Fire1";
            fire2Button = $"{name}_Fire2";
        
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
#endif
        }
    }
}