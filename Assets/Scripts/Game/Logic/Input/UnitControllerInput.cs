using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    public class UnitControllerInput : UnitControllerBaseInput
    {
        [SerializeField]
        private UnitController _controller;

        [SerializeField]
        private UnitControllerInputAsset _inputAsset;

        private Vector3 _moveDirection;

        private Vector3 _fireDirection;

        private bool _isFiring1;

        private bool _isFiring2;

        public override Vector3 MoveDirection => _moveDirection;

        public override Vector3 FireDirection => _fireDirection;

        public override bool IsFiring1
        {
            get => _isFiring1;
            set => _isFiring1 = value;
        }

        public override bool IsFiring2
        {
            get => _isFiring2;
            set => _isFiring2 = value;
        }

        public void Update()
        {
            var x = Input.GetAxis(_inputAsset.moveHorizontalAxis);
            var y = Input.GetAxis(_inputAsset.moveVerticalAxis);
            
            _moveDirection= new Vector3(x, 0, y);
            
           //  var fx = Input.GetAxis(_inputAsset.fireHorizontalAxis);
            // var fy = Input.GetAxis(_inputAsset.fireVerticalAxis);
            bool firing1 = Input.GetButtonDown(_inputAsset.fire1Button);
            bool firing2 = Input.GetButtonDown(_inputAsset.fire2Button);

            var fx = 0;
            var fy = 0;
            
//            if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
//            {
//                fx = Input.GetAxis(_inputAsset.fireHorizontalAxis + "Mac");
//                fy = Input.GetAxis(_inputAsset.fireVerticalAxis + "Mac");
//                firing1 = Input.GetButtonDown(_inputAsset.fire1Button + "Mac");
//                firing2 = Input.GetButtonDown(_inputAsset.fire2Button + "Mac");
//            }

            _isFiring1 = firing1 || _isFiring1;
            _isFiring2 = firing2 || _isFiring2;
        
            _fireDirection = new Vector3(fx, 0, fy);
        }
    }
}