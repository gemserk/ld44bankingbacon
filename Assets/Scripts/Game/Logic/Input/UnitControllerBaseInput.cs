using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    public abstract class UnitControllerBaseInput : MonoBehaviour
    {
        public abstract Vector3 MoveDirection { get; }

        public abstract Vector3 FireDirection { get; }
        
        public abstract bool IsFiring1 { get; set; }
        
        public abstract bool IsFiring2 { get; set; }
    }
}