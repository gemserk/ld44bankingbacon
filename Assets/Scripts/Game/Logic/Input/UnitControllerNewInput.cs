using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    public class UnitControllerNewInput : UnitControllerBaseInput
    {
        [SerializeField]
        private UnitNewInputSingleton inputMaster;

        [SerializeField] 
        private UnitControllerInputAsset _keyboardControls;
        
        private Vector2 _moveDirection;
        private Vector2 _targetDirection;

        public override Vector3 MoveDirection => new Vector3(_moveDirection.x, 0, _moveDirection.y);

        public override Vector3 FireDirection => new Vector3(_targetDirection.x, 0, _targetDirection.y);

        public override bool IsFiring1 { get; set; }
        public override bool IsFiring2 { get; set; }

        private int inputPlayerId;
        
        private void Start()
        {
            inputPlayerId = inputMaster.RegisterPlayer();
        }

        private void Update()
        {
            _moveDirection = new Vector2();
            _targetDirection = new Vector2();

            var gamepad = inputMaster.GetGamepad(inputPlayerId);
            
            if (gamepad != null)
            {
                _moveDirection = gamepad.leftStick.ReadValue();
                _targetDirection = gamepad.rightStick.ReadValue();
                IsFiring1 = gamepad.rightShoulder.wasPressedThisFrame || IsFiring1;
                IsFiring2 = gamepad.leftShoulder.wasPressedThisFrame || IsFiring2;
            } else if (_keyboardControls != null)
            {
                var mx = Input.GetAxis(_keyboardControls.moveHorizontalAxis);
                var my = Input.GetAxis(_keyboardControls.moveVerticalAxis);
                _moveDirection = new Vector2(mx, my);
                
                IsFiring1 = Input.GetButtonDown(_keyboardControls.fire1Button) || IsFiring1;
                IsFiring2 = Input.GetButtonDown(_keyboardControls.fire2Button) || IsFiring2;
            }
            
        }
    }
}