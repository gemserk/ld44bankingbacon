using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    [CreateAssetMenu(menuName = "Gemserk/UnitController")]
    public class UnitDefinitionAsset : ScriptableObject
    {
        public float mass;

        public float maxSpeed;

        public float acceleration;

        public float health;

        public float dashDuration;
        public float dashSpeed;
        public bool dashUsesMoveDirection;
        public bool dashUsesMagnitude;
        public int dashCost;

        
    }
}