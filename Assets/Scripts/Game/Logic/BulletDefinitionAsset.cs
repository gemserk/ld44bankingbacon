using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    [CreateAssetMenu(menuName = "Gemserk/Bullet")]
    public class BulletDefinitionAsset : ScriptableObject
    {
        public float speed;
    }
}