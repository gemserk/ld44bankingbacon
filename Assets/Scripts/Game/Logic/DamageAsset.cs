using System;
using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    [Serializable]
    public class DamageAsset
    {
        public float damage;
    }
}