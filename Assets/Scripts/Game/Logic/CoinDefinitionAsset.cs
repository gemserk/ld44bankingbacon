﻿using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    [CreateAssetMenu(menuName = "Gemserk/Coin Definition")]
    public class CoinDefinitionAsset : ScriptableObject
    {
        public int value;
    }
}
