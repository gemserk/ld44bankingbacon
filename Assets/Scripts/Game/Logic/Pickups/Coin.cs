﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Gemserk.LD44.Game.Logic.Pickups
{
    public class Coin : Pickup
    {
        public UnityEvent hitUnitEvent;

        [SerializeField]
        private CoinDefinitionAsset _coinDefinition;

        private int _coins;

        public int Coins => _coins;

        [SerializeField]
        private Rigidbody _body;

        [SerializeField]
        private float _disabledTime;

        [SerializeField]
        private Collider _pickupTrigger;

        [SerializeField]
        private float _spawnImpulse;

        [SerializeField]
        private float _spawnUpOffset = 4.0f;

        private void Start()
        {
            _coins = _coinDefinition.value;
        }
        
        // to override, just in case...
        public void SetCoins(int coins)
        {
            _coins = coins;
        }
        
        public void Hit(Collider other)
        {
            var otherUnit = other.GetComponentInParent<UnitController>();
            if (otherUnit != null)
            {
                otherUnit.PickUp(this);
                hitUnitEvent.Invoke();
            }
        }

        public void Fire(Vector3 position)
        {
            Fire(position, Vector3.up, _spawnImpulse);
        }
        
        public void Fire(Vector3 position, Vector3 up, float impulse)
        {
            transform.position = position;

            var randomInSphere = UnityEngine.Random.insideUnitSphere + up.normalized * _spawnUpOffset;

            var target = randomInSphere;
            
            _body.AddForce(target.normalized * impulse, ForceMode.Impulse);
            _body.AddRelativeTorque(UnityEngine.Random.insideUnitSphere, ForceMode.Impulse);
            
            StartCoroutine(FireState());
        }

        public IEnumerator FireState()
        {
            _pickupTrigger.enabled = false;
            yield return new WaitForSeconds(_disabledTime);
            _pickupTrigger.enabled = true;
        }
    }
}
