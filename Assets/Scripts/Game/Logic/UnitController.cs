using Gemserk.LD44.Game.Logic.Pickups;
using UnityEngine;
using UnityEngine.Events;

namespace Gemserk.LD44.Game.Logic
{
    public class UnitController : MonoBehaviour
    {
        [SerializeField] 
        private UnitDefinitionAsset _unitControllerAsset;
    
        [SerializeField]
        private Rigidbody _body;

        [SerializeField]
        private UnitWeaponAsset _unitWeapon1;
        [SerializeField]
        private UnitWeaponAsset _unitWeapon2;

        // this could be delegated to the model...

        [SerializeField]
        private UnitControllerBaseInput _input;
        
        [SerializeField]
        private UnitModelController _model;

        [SerializeField]
        private GameObject _coinPrefab;

        private Vector3 _lookingDirection;
        
        public UnityEvent receivedDamageEvent;

        public Health health = new Health();

        public bool dashing;
        public float dashDurationCounter;

        public UnityEvent startDashEvent;
        public UnityEvent endDashEvent;

        public UnityEvent deathEvent;

        public UnityEvent pickupEvent;

        public UnityEvent fireEvent;
        
//        private CoinSpawner _coinSpawner = new CoinSpawner();

        public Vector3 LookingDirection
        {
            get => _lookingDirection;
            set => _lookingDirection = value;
        }
        
        private void Start()
        {
            _body.mass = _unitControllerAsset.mass;
            health.SetMax(_unitControllerAsset.health);
        }
    
        // Update is called once per frame
        private void FixedUpdate()
        {
#if UNITY_EDITOR
            _body.mass = _unitControllerAsset.mass;
#endif

            if (dashing)
            {
                dashDurationCounter -= Time.deltaTime;
                if (dashDurationCounter < 0)
                {
                    dashing = false;
                    endDashEvent.Invoke();
                }
            }

            if (_input.FireDirection.sqrMagnitude > 0.001f && !dashing)
            {
                _lookingDirection = _input.FireDirection;
            } else if (_input.MoveDirection.sqrMagnitude > 0.001f)
            {
                _lookingDirection = _input.MoveDirection;
            }

            if (!dashing)
            {
                var velocity = _input.MoveDirection * _unitControllerAsset.acceleration * Time.deltaTime;
                velocity = Vector3.ClampMagnitude(velocity, _unitControllerAsset.maxSpeed);
                _body.velocity = new Vector3(velocity.x, _body.velocity.y, velocity.z);
            }

            if (!dashing && _input.IsFiring2 && health.Current > _unitControllerAsset.dashCost)
            {
                Vector3 dashDirection;

                if (_unitControllerAsset.dashUsesMoveDirection)
                {
                    dashDirection = _input.MoveDirection.magnitude > 0.1 ? _input.MoveDirection : _model.LookingDirection;
                }
                else
                {
                    dashDirection = _model.LookingDirection;
                }

                if (!_unitControllerAsset.dashUsesMagnitude)
                {
                    dashDirection.Normalize();
                }
                
                _body.velocity = dashDirection * _unitControllerAsset.dashSpeed;
                dashDurationCounter = _unitControllerAsset.dashDuration;
                dashing = true;
                startDashEvent.Invoke();
                for (var i = 0; i < _unitControllerAsset.dashCost; i++)
                {
                    health.Add(-1);
                    SpawnCoin();
                }
            }

            // can't fire if not enough life
            if (!dashing && health.Current >= _unitWeapon1.selfDamage.damage)
            {
                var p = _model.GetAttackAttachPoint();

                DamageAsset selfDamage = null;


                if (_input.IsFiring1)
                {
                    selfDamage = _unitWeapon1.Fire(this, p.position, p.forward);
                    fireEvent.Invoke();
                }

                if (selfDamage != null && selfDamage.damage > 0.0f)
                {
                    health.Add(-selfDamage.damage);
                    // avoid dying
                    if (health.Current <= 0.5f)
                    {
                        health.Current = 0.5f;
                    }
                }
            }
            
            if (_input.IsFiring1)
            {
                _input.IsFiring1 = false;
            }

            if (_input.IsFiring2)
            {
                _input.IsFiring2 = false;
            }
            
            _model.LookingDirection = _lookingDirection;
        }

        public void ProjectileHit(UnitController owner, DamageAsset bulletDamage)
        {
            if (owner == this)
            {
                return;
            }

            OnDamageReceived(bulletDamage.damage);
        }

        private void OnDamageReceived(float damage)
        {
            health.Add(-damage);
            receivedDamageEvent.Invoke();

            if (health.Current <= 0.01f)
            {
                // disable input
                _input.enabled = false;
                var colliders = GetComponentsInChildren<Collider>();
                foreach (var collider in colliders)
                {
                    collider.enabled = false;
                }
                
                _model.SetDeath();
                
                deathEvent.Invoke();
                
                // _model.
            }

            SpawnCoin();
        }

        private void SpawnCoin()
        {
            var coinSpawnPosition = _model.GetSpawnCoinsAttachPoint();

            var coinObject = GameObject.Instantiate(_coinPrefab);
            var coin = coinObject.GetComponentInChildren<Coin>();

            coin.Fire(coinSpawnPosition.position);
        }

        public void PickUp(Pickup pickup)
        {
            var coin = pickup as Coin;
            if (coin != null)
            {
                health.Add(coin.Coins);
                pickupEvent.Invoke();
            }
        }
    }
}
