using Gemserk.LD44.Game.Logic.Weapons;
using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    
    public abstract class UnitWeaponAsset : ScriptableObject
    {
        public float reloadTime;
        public DamageAsset selfDamage;

        public abstract DamageAsset Fire(UnitController unit, Vector3 position, Vector3 direction);
    }
}