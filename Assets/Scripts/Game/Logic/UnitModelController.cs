﻿using UnityEngine;

namespace Gemserk.LD44.Game.Logic
{
    public class UnitModelController : MonoBehaviour
    {
        [SerializeField]
        private Animator _animator;
  
        [SerializeField]
        private Rigidbody _body;

        [SerializeField]
        private string _runParameter = "Running";

        [SerializeField]
        private string _deadParameter = "Dead";

        [SerializeField]
        private float _minVelocityToAnimate = 0.001f;

        [SerializeField]
        private Transform _attackAttachPoint;

        [SerializeField]
        private Transform _spawnCoinsAttachPoint;

        [SerializeField]
        private Transform _modelTransform;

        private Vector3 _lookingDirection;

        [SerializeField]
        private GameObject _selector;

        public Vector3 LookingDirection
        {
            get => _lookingDirection;
            set => _lookingDirection = value;
        }

        public void SetDeath()
        {
            _selector.gameObject.SetActive(false);
            _animator.SetBool(_deadParameter, true); 
        }
        
        // Update is called once per frame
        private void Update()
        {
            var running = _body.velocity.sqrMagnitude > _minVelocityToAnimate;
            _animator.SetBool(_runParameter, running);
            _modelTransform.LookAt(_modelTransform.position + _lookingDirection);
       }

        public Transform GetAttackAttachPoint()
        {
            return _attackAttachPoint;
        }
        
        public Transform GetSpawnCoinsAttachPoint()
        {
            return _spawnCoinsAttachPoint;
        }
    }
}
