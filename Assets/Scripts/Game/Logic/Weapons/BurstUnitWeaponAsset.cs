using UnityEngine;

namespace Gemserk.LD44.Game.Logic.Weapons
{
    [CreateAssetMenu(menuName = "Gemserk/Weapons/Burst Unit Weapon")]
    public class BurstUnitWeaponAsset : UnitWeaponAsset
    {
        public Bullet _bulletPrefab;
        public DamageAsset bulletDamage;

        public float spread;
        public float cantBullets;

        public override DamageAsset Fire(UnitController unit, Vector3 position, Vector3 direction)
        {
            var baseDirection = direction;
            for (int i = 0; i < cantBullets; i++)
            {
                var rotation = Quaternion.AngleAxis(UnityEngine.Random.Range(-spread, spread), Vector3.up);
                var newDirection = rotation * baseDirection;
                
                var bullet = GameObject.Instantiate(_bulletPrefab);
                bullet.Fire(unit, position, newDirection, bulletDamage);
            }
            
            return selfDamage;
        }
    }
}