using Gemserk.LD44.Game.Logic.Pickups;
using UnityEngine;
using UnityEngine.Events;

namespace Gemserk.LD44.Game.Logic.Weapons
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody _rigidbody;

        [SerializeField]
        private BulletDefinitionAsset _bulletDefinition;
        
        private DamageAsset _bulletDamage;
        
        private UnitController _owner;
        
        public UnityEvent hitObstacleEvent;
        public UnityEvent hitUnitEvent;

        [SerializeField]
        private float _fireTorqueImpulse;

        [SerializeField]
        private GameObject _coinPrefab;

        private bool _hitProcessed;

        public void Fire(UnitController owner, Vector3 position, Vector3 direction, DamageAsset bulletDamage)
        {
            _owner = owner;
            transform.position = position;
            transform.LookAt(position + direction, owner.transform.up);
            _rigidbody.velocity = direction.normalized * _bulletDefinition.speed;
            _bulletDamage = bulletDamage;
            
            _rigidbody.AddRelativeTorque(UnityEngine.Random.insideUnitSphere * _fireTorqueImpulse, ForceMode.Impulse);
        }

        public void Hit(Collider other)
        {
            if (_hitProcessed)
                return;

            _hitProcessed = true;
            
            var otherUnit = other.GetComponentInParent<UnitController>();
            if (otherUnit != null)
            {
                otherUnit.ProjectileHit(_owner, _bulletDamage);
                hitUnitEvent.Invoke();
            }
            else
            {
                OnObstacleHit();
                hitObstacleEvent.Invoke();
            }
           
        }

        public void OnObstacleHit()
        {
            // spawn coin
            if (_coinPrefab != null)
            {
                var coinObject = GameObject.Instantiate(_coinPrefab);
                var coin = coinObject.GetComponentInChildren<Coin>();

                coin.Fire(transform.position - _rigidbody.velocity.normalized * 0.5f, 
                    -_rigidbody.velocity, 1.0f);

            }
        }

    }
}