using UnityEngine;

namespace Gemserk.LD44.Game.Logic.Weapons
{
    [CreateAssetMenu(menuName = "Gemserk/Weapons/Simple Unit Weapon")]
    public class SimpleUnitWeaponAsset : UnitWeaponAsset
    {
        public Bullet _bulletPrefab;
        public DamageAsset bulletDamage;

        public override DamageAsset Fire(UnitController unit, Vector3 position, Vector3 direction)
        {
            var bullet = GameObject.Instantiate(_bulletPrefab);     
            bullet.Fire(unit, position, direction, bulletDamage);
            return selfDamage;
        }
    }
}