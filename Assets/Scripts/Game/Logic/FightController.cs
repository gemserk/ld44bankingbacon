﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using Gemserk.LD44.Game.Flow;
using Gemserk.LD44.Game.UI;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gemserk.LD44.Game.Logic
{
    public class FightController : MonoBehaviour
    {
        private enum GameState
        {
            Starting,
            Playing,
            Finishing,
            Finished
        }
        
        private FightStageRoot _stageRoot;

        [SerializeField]
        private List<UnitController> unitPrefabs;

        //[SerializeField]
        //private CinemachineTargetGroup _targetGroup;

        [SerializeField]
        private GameOutcomeScreen _gameOutcome;

        [SerializeField]
        private GameHud _gameHud;

        private List<UnitController> _players = new List<UnitController>();

        private GameState _gameState;

        [SerializeField]
        private GameObject _uiContainer;
        
        [SerializeField]
        private UnitNewInputSingleton _inputMaster;
        
        public void Start()
        {
            _inputMaster.Reset();
            _stageRoot = GameObject.FindObjectOfType<FightStageRoot>();
            _stageRoot.Initialize();
            Initialize();
        }

        private void Initialize()
        {
            Debug.Log($"Fight: {PlayConfig.cantPlayers} players");
            //_targetGroup.RemoveMember(this.transform);
            for (var i = 0; i < PlayConfig.cantPlayers; i++)
            {
                var unitPrefab = unitPrefabs[i];
                var spawnPos = _stageRoot.startPositions[i];

                var player = GameObject.Instantiate(unitPrefab, spawnPos.transform.position, Quaternion.identity);
                var unit = player.GetComponent<UnitController>();

                unit.LookingDirection = spawnPos.transform.forward;
                
                //_targetGroup.AddMember(player.transform, 1, 0.5f);
                _players.Add(player);
            }

            _gameState = GameState.Playing;
            
            _uiContainer.SetActive(true);
            _gameOutcome.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (_gameState == GameState.Playing)
            {
                _gameHud.UpdatePlayersInfo(_players);
                
                var alivePlayers = 0;

                for (var i = 0; i < PlayConfig.cantPlayers; i++)
                {
                    if (_players[i].health.Current > 0.01f)
                        alivePlayers++;
                }

                if (alivePlayers <= 1)
                {
                    OnGameFinished();
                }
            }
            
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.R))
            {
                SceneLists.Load().LoadMenu();
            }
            
            if (_gameOutcome.gameObject.activeInHierarchy && _inputMaster.Gamepads != null)
            {
                var start = _inputMaster.Gamepads.Any(g => g != null && g.startButton.wasReleasedThisFrame);
                var select = _inputMaster.Gamepads.Any(g => g != null && g.selectButton.wasReleasedThisFrame);
                
                if (start)
                {
                    OnGameRestart();
                } else if (select)
                {
                    SceneLists.Load().LoadMenu();
                }
            }
        }

        private void OnGameFinished()
        {
            _gameState = GameState.Finished;
            
            // disable input 
            // make players invulnerable (to avoid a flying bullet to kill another player?)
            
            // set stuff
            // start coroutines if we want something cool
            _gameOutcome.gameObject.SetActive(true);

            var gameOutcomeData = new GameOutcomeData[PlayConfig.cantPlayers];

            for (var i = 0; i < PlayConfig.cantPlayers; i++)
            {
                gameOutcomeData[i] = new GameOutcomeData()
                {
                    player = i,
                    win = _players[i].health.Current > 0,
                };
            }
            
            _gameOutcome.OnGameCompleted(new GameOutcome()
            {
                playersData = gameOutcomeData
            }, OnGameRestart);
        }

        private void OnGameRestart()
        {
            var go = new GameObject();
            GameObject.DontDestroyOnLoad(go);
            
            var temp = go.AddComponent<GameRestarter>();
            temp.StartCoroutine(RestartGame(go));
        }

        private IEnumerator RestartGame(GameObject go)
        {
            var scenesToReload = new List<string>();

            for (var i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                scenesToReload.Add(scene.name);
            }

            SceneManager.LoadScene(scenesToReload[0], LoadSceneMode.Single);

            for (var i = 1; i < scenesToReload.Count; i++)
            {
                var scene = scenesToReload[i];
                SceneManager.LoadScene(scene, LoadSceneMode.Additive);
               // yield return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
            }

            GameObject.Destroy(go);

            yield return null;
        }
        
        private IEnumerator GameFinished()
        {
            yield return null;
        }
        
        
    }
}

