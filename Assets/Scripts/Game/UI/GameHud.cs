﻿using System.Collections.Generic;
using Gemserk.LD44.Game.Logic;
using TMPro;
using UnityEngine;

public class GameHud : MonoBehaviour
{
    [SerializeField]
    private List<TextMeshProUGUI> _labels;

    public void UpdatePlayersInfo(List<UnitController> players)
    {
        for (var i = 0; i < _labels.Count; i++)
        {
            if (i < players.Count)
            {
                _labels[i].gameObject.SetActive(true);
                _labels[i].text = Mathf.FloorToInt(players[i].health.Current).ToString();
            }
            else
            {
                _labels[i].gameObject.SetActive(false);
            }
        }
    }
}
