﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gemserk.LD44.Game.UI
{
    public struct GameOutcomeData
    {
        public int player;
        public bool win;
    }
    
    public struct GameOutcome
    {
        public GameOutcomeData[] playersData;
    }
    
    public class GameOutcomeScreen : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _text;

        [SerializeField]
        private Button _restartButton;
        
        [SerializeField]
        private string _victoryFormat = "{0} Wins!";
    
        [SerializeField]
        private string _drawFormat = "Draw!";

        private Action _onRestartCallback;

        [SerializeField]
        private List<string> _playerNames = new List<string>();

        [SerializeField]
        private List<Color> _playerColors = new List<Color>();
        
        private void Start()
        {
            _restartButton.onClick.AddListener(delegate
            {
                _onRestartCallback?.Invoke();
            });
        }

        public void OnGameCompleted(GameOutcome gameOutcome, Action onRestartCallback)
        {
            var winPlayer = -1;

            foreach (var playerData in gameOutcome.playersData)
            {
                if (!playerData.win)
                    continue;
                
                winPlayer = playerData.player;
            }
            
            if (winPlayer >= 0)
            {
                _text.text = string.Format(_victoryFormat, _playerNames[winPlayer]);
                _text.color = _playerColors[winPlayer];
            }
            else
            {
                _text.text = string.Format(_drawFormat);
                _text.color = Color.white;
            }

            _onRestartCallback = onRestartCallback;
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                _onRestartCallback?.Invoke();
            }
        }
    }
}
