using System;
using System.Linq;
using Gemserk.LD44.Game.Logic;
using UnityEngine;

namespace Gemserk.LD44.Game.Flow
{
    public class MainMenuController : MonoBehaviour
    {
        public AudioSource music;
        private static bool firstTime = true;

        [SerializeField]
        private UnitNewInputSingleton _inputMaster;

        private void Awake()
        {
            if (firstTime)
            {
                music.Play();
                GameObject.DontDestroyOnLoad(music.gameObject);

               
            }

            _inputMaster.Reset();
            firstTime = false;
        }

        public void Load(int players)
        {
            PlayConfig.cantPlayers = players;
            SceneLists.Load().LoadFight();
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Load(2);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Load(3);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                Load(4);
            }

            if (_inputMaster.Gamepads != null)
            {
                var start = _inputMaster.Gamepads.Any(g => g != null && g.startButton.wasReleasedThisFrame);
                if (start)
                {
                    var gamepadsCount = _inputMaster.Gamepads.Count(g => g != null);
                    Load(Mathf.Max(gamepadsCount, 2));
                }
            }
            else
            {
                Debug.Log("gamepads is null");
            }

        }
    }
}