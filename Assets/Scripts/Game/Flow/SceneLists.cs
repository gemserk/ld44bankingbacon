using System.Collections.Generic;
using Gemserk.MultiSceneManager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gemserk.LD44.Game.Flow
{
    [CreateAssetMenu(menuName = "Gemserk/Scenes/SceneList")]
    public class SceneLists : ScriptableObject
    {
        
        [SerializeField]
        private List<SceneField> menuScenes;
        
        [SerializeField]
        private List<SceneField> fightScenes;

        public void LoadMenu()
        {
            LoadScenes(menuScenes);
        }
        
        public void LoadFight()
        {
            LoadScenes(fightScenes);
        }
        
        private void LoadScenes(List<SceneField> scenesToLoad)
        {
            for (var index = 0; index < scenesToLoad.Count; index++)
            {
                var scene = scenesToLoad[index];
                var additive = index == 0 ? LoadSceneMode.Single : LoadSceneMode.Additive;
                SceneManager.LoadScene(scene.SceneName, additive);
                Debug.Log($"Loading scene: {scene.SceneName} in {additive}");
            }
        }

        public static SceneLists Load()
        {
            return Resources.Load<SceneLists>("SceneLists");
        }
    }
}